<?php

namespace minBlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function adminAction() {
        return $this->render('minBlogBundle:Default:index.html.twig', array(
                    'page' => 'Admin'
        ));
    }

    public function blogAction() {
        return $this->render('minBlogBundle:Default:index.html.twig', array(
                    'page' => 'BlogPage'
        ));
    }

    public function editArticleAction($id) {
         return $this->render('minBlogBundle:Default:index.html.twig', array(
            'page' => 'Edit Article',
             'id' => $id
        ));
    }

    public function createArticleAction() {
         return $this->render('minBlogBundle:Default:index.html.twig', array(
            'page' => 'Create Acticle'
        ));
    }

    public function editCategoryAction($id) {
         return $this->render('minBlogBundle:Default:index.html.twig', array(
            'page' => 'Edir Category',
             'id' => $id
        ));
    }

    public function createCategoryAction() {
         return $this->render('minBlogBundle:Default:index.html.twig', array(
            'page' => 'Create Category'
        ));
    }
    
    public function loginAction(Request $request) {
          $authenticationUtils = $this->get('security.authentication_utils');

    // get the login error if there is one
    $error = $authenticationUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authenticationUtils->getLastUsername();

    return $this->render('minBlogBundle:Default:login.html.twig', array(
        'last_username' => $lastUsername,
        'error'         => $error,
    ));
    }

}
