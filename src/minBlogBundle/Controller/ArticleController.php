<?php

namespace minBlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use minBlogBundle\Entity\Article;
use minBlogBundle\Form\ArticleType;

/**
 * Article controller.
 *
 */
class ArticleController extends Controller {

    /**
     * Lists all Article entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('minBlogBundle:Article')->findAll();

        return $this->render('@minBlogBundle/article/index.html.twig', array(
                    'articles' => $articles,
        ));
    }

    /**
     * Creates a new Article entity.
     *
     */
    public function newAction(Request $request) {
        $article = new Article();
        $form = $this->createForm('minBlogBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setCreateAt(new \DateTime());
            $this->uploadImage($article);
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('admin_blog_show', array('id' => $article->getId()));
        }

        return $this->render('@minBlogBundle/article/new.html.twig', array(
                    'article' => $article,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Article entity.
     *
     */
    public function showAction(Article $article) {
        $deleteForm = $this->createDeleteForm($article);
        $imagePath = $article->getImage();
        return $this->render('@minBlogBundle/article/show.html.twig', array(
                    'article' => $article,
                    'delete_form' => $deleteForm->createView(),
                    'imagePath' => $imagePath
        ));
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     */
    public function editAction(Request $request, Article $article) {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('minBlogBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);
        $imagePath = $article->getImage();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $article->setUpdateAt(new \DateTime());
            $this->uploadImage($article);
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('admin_blog_edit', array('id' => $article->getId()));
        }

        return $this->render('@minBlogBundle/article/edit.html.twig', array(
                    'article' => $article,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'imagePath' => $imagePath
        ));
    }

    /**
     * Deletes a Article entity.
     *
     */
    public function deleteAction(Request $request, Article $article) {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('admin_blog_index');
    }

    public function deleteImageAction(Request $request, Article $article) {

        $imagePath = $article->getImage();
        $file_path = $this->getParameter('images_directory') . "/" . $imagePath;
        if (file_exists($file_path)) {
            unlink($file_path);
            $article->setImage(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
        }

        return $this->redirectToRoute('admin_blog_edit', array('id' => $article->getId()));
    }

    /**
     * Creates a form to delete a Article entity.
     *
     * @param Article $article The Article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_blog_delete', array('id' => $article->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function showCategoryAction(\minBlogBundle\Entity\Category $category) {
        $articles = $category->getArticles();

        return $this->render('@minBlogBundle/article/index.html.twig', array(
                    'articles' => $articles,
                    'category' => $category->getName()
        ));
    }

    private function uploadImage($article) {

        $file = $article->getImage();
        if ($file) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                    $this->getParameter('images_directory'), $fileName
            );
        } else {
            $fileName = null;
        }
        $article->setImage($fileName);
    }

}
