<?php

namespace minBlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ArticleType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title')
                ->add('slug')
                ->add('shortDescription')
                ->add('content')
                ->add('date', DateType::class, array(
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => ['class' => 'js-datepicker']))
                ->add('category', EntityType::class, array(
                    'class' => 'minBlogBundle:Category'
                ))                
                ->add('image',  FileType::class,array(
                    'data_class' => null,
                    'required'=>false))
                ->add('metaAuthor')
                ->add('metaKeywords')
                ->add('metaDescription')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'minBlogBundle\Entity\Article'
        ));
    }

}
