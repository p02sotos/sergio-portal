<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        // replace this example code with whatever you need
        $repository = $this->getDoctrine()->getRepository('minBlogBundle:Article');
        $query = $repository->createQueryBuilder('a')->orderBy('a.date', 'DESC')->getQuery();
        $lastArticle = $query->setMaxResults(1)->getOneOrNullResult();
        $imagePath = $lastArticle->getImage();
        $form = $this->createContactForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('homepage');
        }

        return $this->render('@AppBundle/pages/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'article' => $lastArticle,
                    'imagePath' => $imagePath,
                    'title' => "Sergio Soriano Personal Web",
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/blog/{id}(", name="blogArticle")
     */
    public function blogArticleAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository('minBlogBundle:Article');
        $article = $repository->findOneById($id);
        $imagePath = $article->getImage();
        $form = $this->createContactForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('blogArticle', array('id'=> $id));
        }
        return $this->render('@AppBundle/pages/blogArticle.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'article' => $article,
                    'imagePath' => $imagePath,
                    'title' => "Sergio Soriano - Blog - " . $article->getId(),
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function blogAction(Request $request) {

        $categories = $this->getDoctrine()->getRepository('minBlogBundle:Category')->findAll();
        $repository = $this->getDoctrine()->getRepository('minBlogBundle:Article');
        $articles = $repository->findBy(array(), array('date' => 'DESC'));
        $form = $this->createContactForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('blog');
        }
        return $this->render('@AppBundle/pages/blog.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'articles' => $articles,
                    'categories' => $categories,
                    'title' => "Sergio Soriano - Blog",
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/{id}", name="category")
     */
    public function categoryAction(Request $request, $id) {
        $form = $this->createContactForm();
        $categories = $this->getDoctrine()->getRepository('minBlogBundle:Category')->findAll();
        $query = $this->getDoctrine()->getManager()
                        ->createQuery(
                                'SELECT c, a FROM minBlogBundle:Category c
                                    JOIN c.articles a
                                    WHERE c.id = :id
                                    ORDER BY a.date DESC'
                        )->setParameter('id', $id);
        $articles = $query->getResult();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('category',array('id'=> $id));
        }
        return $this->render('@AppBundle/pages/blog.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'articlesC' => $articles,
                    'categories' => $categories,
                    'title' => "Sergio Soriano - Blog - Category - " . $id,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume", name="resume")
     */
    public function resumeAction(Request $request) {
        // replace this example code with whatever you need

        $form = $this->createContactForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('resume');
        }
        return $this->render('@AppBundle/pages/resume.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'title' => "Sergio Soriano - Resume",
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function portfolioAction(Request $request) {
        $form = $this->createContactForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->sendMessage($data);
            return $this->redirectToRoute('portfolio');
        }
        return $this->render('@AppBundle/pages/portfolio.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'title' => "Sergio Soriano - Portfolio",
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/install", name="install")
     */
    public function createAction(Request $request) {
        $user = $this->getDoctrine()->getRepository('minBlogBundle:User')->findOneByUsername('admin');
        $category = $this->getDoctrine()->getRepository('minBlogBundle:Category')->findOneByName('uncategory');
        $em = $this->getDoctrine()->getManager();
        if (!$user) {
            $user = new \minBlogBundle\Entity\User();
            $plainPassword = 'caca66';
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $user->setUsername("admin");
            $user->setEmail("p02sotos@hotmail.com");
            $user->setIsActive(true);
            $user->setPassword($encoded);
            $em->persist($user);
        }
        if (!$category) {
            $category = new \minBlogBundle\Entity\Category();
            $category->setName("uncategory");
            $category->setDescription("No categories");
            $category->setCreateAt(new \DateTime());

            $em->persist($category);
        }
        $em->flush();

        return $this->render('default/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'user' => $user
        ]);
    }

    private function createContactForm() {

        $form = $this->createFormBuilder()
                ->add('name', TextType::class)
                ->add('email', EmailType::class)
                ->add('message', TextareaType::class)
                ->add('save', SubmitType::class, array('label' => 'Send Message', 'attr' => array('class' => 'special')))
                ->getForm();
        return $form;
    }

    private function sendMessage($data) {

        $messageMail = \Swift_Message::newInstance()
                ->setSubject('Mensaje Recibido de ' . $data['name'])
                ->setFrom('no-reply@comunidadescordoba.es')
                ->setTo('p02sotos@gmail.com')
                ->setBody(
                $this->renderView(
                        '@AppBundle/partials/contactMail.html.twig', array('data' => $data)
                ), 'text/html'
        );
        $this->get('mailer')->send($messageMail);
    }

}
